/*
    SPDX-FileCopyrightText: 2008 Peter Hedlund <peter.hedlund@kdemail.net>
    SPDX-License-Identifier: LGPL-2.0-only
*/

#include "prefgeneral.h"

PrefGeneral::PrefGeneral(QWidget *parent) : QWidget(parent)
{
  setupUi(this);
}
